﻿using QAApplicationProj.Helpers;
using Xunit;
using System;

namespace QAApplicationProj.Tests.Helpers
{
    public class FareHelperTests
    {
        // Return values
        [Fact]
        public void AddFares_Simple_Pass()
        {
            Assert.Equal(2, FareHelper.AddFares(1, 1));
        }

        [Fact]
        public void AddFares_Simple_Fail()
        {
            Assert.NotEqual(0, FareHelper.AddFares(1, 1));
        }

        [Fact]
        public void AddFares_Max_Param_value()
        {
            Assert.Equal(decimal.MaxValue, FareHelper.AddFares(0, decimal.MaxValue));
        }

        // Exceptions
        [Fact]
        public void AddFares_Bad_First_Param()
        {
            Exception ex = Assert.Throws<ArgumentOutOfRangeException>(() => FareHelper.AddFares(-1, 1));
            Assert.Equal("The first fare must be positive.\r\nParameter name: firstFare", ex.Message);
        }

        [Fact]
        public void AddFares_Bad_Second_Param()
        {
            Exception ex = Assert.Throws<ArgumentOutOfRangeException>(() => FareHelper.AddFares(1, -1));
            Assert.Equal("The second fare must be positive.\r\nParameter name: secondFare", ex.Message);
        }

        [Fact]
        public void AddFares_Max_Param_value_Overload()
        {
            Assert.Throws<OverflowException>(() => FareHelper.AddFares(1, decimal.MaxValue));
        }

    }
}
