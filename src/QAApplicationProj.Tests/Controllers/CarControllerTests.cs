﻿using QAApplicationProj.Controllers;
using QAApplicationProj.Responses;
using QAApplicationProj.Data.Entities;
using QAApplicationProj.Data.Repositories.Interfaces;
using System;
using Xunit;
using Moq;

namespace QAApplicationProj.Tests.Controllers
{
    public class CarControllerTests
    {
        private readonly CarController _controller;
        private Mock<ICarRepository> mockCarRepo;

        public CarControllerTests()
        {
            mockCarRepo = new Mock<ICarRepository>();
           _controller = new CarController(mockCarRepo.Object); 
        }

        [Fact]
        public void CreateCar_Null_Request()
        {
            var response = new CreateCarResponse();
            
            response = _controller.CreateCar(null);
            Assert.Equal("The request did not pass validation.", response.Message);
        }

        [Fact]
        public void CreateCar_Invalid_Request_Empty()
        {
            var request = new Requests.CarRequest();
            var response = new CreateCarResponse();
   
            response = _controller.CreateCar(request);
            Assert.Equal( "The request did not pass validation.", response.Message);
        }

        [Theory]
        [InlineData(null, null, null)]
        [InlineData("make", "model", 0)]
        [InlineData(null, "model", 1999)]
        [InlineData("make", null, 1999)]
        public void CreateCar_Invalid_Request_Params(string make, string model, int year)
        {
            var request  = new Requests.CarRequest();
            var response = new CreateCarResponse();
            
            request.Make = make;
            request.Model = model;
            request.Year = year;

            response = _controller.CreateCar(request);
            Assert.Equal("The request did not pass validation.", response.Message);
        }

        [Fact]
        public void CreateCar_Simple_Request_Pass()
        {
            var request   = new Requests.CarRequest();
            var response  = new CreateCarResponse();

            request.Make  = "Tesla";
            request.Model = "Affordable";
            request.Year  = 3000;

            response = _controller.CreateCar(request);
            Assert.True(response.WasSuccessful);
        }

        [Fact]
        public void CreateCar_Request_Already_Exists()
        {
            var car = new Car();
            car.Make  = "Tesla";
            car.Model = "Affordable";
            car.Year  = 3000;

            mockCarRepo.Setup(cr => cr.Exists(car)).Returns(true);

            var request  = new Requests.CarRequest();
            var response = new CreateCarResponse();

            request.Make  = car.Make;
            request.Model = car.Model;
            request.Year  = car.Year;

            response = _controller.CreateCar(request);
            Assert.Equal("The requested Car already exists.", response.Message);
        }

        [Fact]
        public void CreateCar_Request_Unhandled_Exception()
        {
            var car = new Car();
            car.Make  = "justEnoughData";
            car.Model = "toPassValidation";
            car.Year  = 7777;

            mockCarRepo.Setup(cr => cr.Exists(car)).Throws<ArgumentOutOfRangeException>();

            var request  = new Requests.CarRequest();
            var response = new CreateCarResponse();

            request.Make  = car.Make;
            request.Model = car.Model;
            request.Year  = car.Year;

            response = _controller.CreateCar(request);
            Assert.Equal("An unhandled exception occurred while creating the requested Car.", response.Message);
        }
    }
}
