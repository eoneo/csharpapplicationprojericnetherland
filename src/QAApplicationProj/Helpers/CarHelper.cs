﻿using QAApplicationProj.Requests;

namespace QAApplicationProj.Helpers
{
    public static class CarHelper
    {
        public static bool IsRequestValid(CarRequest request) {
            return !string.IsNullOrEmpty(request?.Make) && !string.IsNullOrEmpty(request.Model) && request.Year > 0;
        }
    }
}
